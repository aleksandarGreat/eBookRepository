package com.aradosevic.eBookRepository.eBookRepository.controller;

import com.aradosevic.eBookRepository.eBookRepository.dto.book.BookDTO;
import com.aradosevic.eBookRepository.eBookRepository.dto.book.UpdateBookDTO;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomException;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomExceptionHandler;
import com.aradosevic.eBookRepository.eBookRepository.model.Book;
import com.aradosevic.eBookRepository.eBookRepository.repository.elasticsearch.BookRepository;
import com.aradosevic.eBookRepository.eBookRepository.service.controller.BookControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/books")
public class BookController {

    @Autowired
    private BookControllerService bookControllerService;

    @Autowired
    private BookRepository bookRepository;

    @GetMapping(value = "/all")
    public ResponseEntity getAllBooks() {
        List<Book> books = new ArrayList<>();
        bookRepository.findAll().forEach(books::add);
        List<BookDTO> bookDTOs = new ArrayList<>();
        for (Book book : books) {
            bookDTOs.add(new BookDTO(book, null));
        }
        return new ResponseEntity<>(bookDTOs, HttpStatus.OK);
    }

    @RequestMapping(path = "/upload", method = RequestMethod.POST)
    public ResponseEntity uploadBook(@RequestParam("file") @NotNull MultipartFile file,
                                     @RequestParam("categoryId") @NotNull long categoryId,
                                     @RequestParam("languageId") @NotNull long languageId) {

        try {
            return new ResponseEntity<>(bookControllerService.uploadBook(file, categoryId, languageId), HttpStatus.OK);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(path = "/update")
    public ResponseEntity updateBook(@RequestBody @Valid UpdateBookDTO updateBookDTO) {
        try {
            bookControllerService.updateBook(updateBookDTO);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/download/{id}", method = RequestMethod.GET)
    public ResponseEntity downloadBook(@PathVariable long id) {

        Book book;
        try {
            book = bookControllerService.downloadBook(id);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }

        File file = new File(book.getFilePath());
        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource;
        try {
            resource = new ByteArrayResource(Files.readAllBytes(path));
        } catch (IOException e) {
            return CustomExceptionHandler.handleException(new CustomException(e.getMessage()), HttpStatus.BAD_REQUEST);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Headers", HttpHeaders.CONTENT_DISPOSITION);
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + book.getName());

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/pdf"))
                .body(resource);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteBook(@PathVariable long id) {
        try {
            bookControllerService.deleteBook(id);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
