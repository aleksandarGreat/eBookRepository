package com.aradosevic.eBookRepository.eBookRepository.controller;

import com.aradosevic.eBookRepository.eBookRepository.dto.category.CreateCategoryDTO;
import com.aradosevic.eBookRepository.eBookRepository.dto.category.UpdateCategoryDTO;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomException;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomExceptionHandler;
import com.aradosevic.eBookRepository.eBookRepository.service.controller.CategoryControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/categories")
public class CategoryController {

    @Autowired
    private CategoryControllerService categoryControllerService;

    @GetMapping(value = "/all")
    public ResponseEntity getAllCategories() {
        return new ResponseEntity<>(categoryControllerService.getAllCategories(), HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public ResponseEntity createCategory(@RequestBody @Valid CreateCategoryDTO createCategoryDTO) {
        try {
            categoryControllerService.createCategory(createCategoryDTO);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity updateCategory(@RequestBody @Valid UpdateCategoryDTO updateCategoryDTO) {
        try {
            categoryControllerService.updateCategory(updateCategoryDTO);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteCategory(@PathVariable long id) {
        try {
            categoryControllerService.deleteCategory(id);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
