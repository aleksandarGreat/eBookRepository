package com.aradosevic.eBookRepository.eBookRepository.controller;

import com.aradosevic.eBookRepository.eBookRepository.dto.user.*;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomException;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomExceptionHandler;
import com.aradosevic.eBookRepository.eBookRepository.service.controller.UserControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/users")
public class UserController {

    @Autowired
    private UserControllerService userControllerService;

    @GetMapping(value = "/all")
    public ResponseEntity getAllUsers() {
        return new ResponseEntity<>(userControllerService.getAllUsers(), HttpStatus.OK);
    }

    @PutMapping(value = "/change")
    public ResponseEntity changeUser(@RequestBody @Valid ChangeUserDTO changeUserDTO) {
        try {
            userControllerService.changeUser(changeUserDTO);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update-user")
    public ResponseEntity updateUser(@RequestBody @Valid UpdateUserDTO updateUserDTO) {
        try {
            userControllerService.updateUser(updateUserDTO);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/update-password")
    public ResponseEntity updatePassword(@RequestBody @Valid UpdatePasswordDTO updatePasswordDTO) {
        try {
            userControllerService.updatePassword(updatePasswordDTO);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public ResponseEntity createUser(@RequestBody @Valid CreateUserDTO createUserDTO) {
        try {
            userControllerService.createUser(createUserDTO);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteUser(@PathVariable long id) {
        try {
            userControllerService.deleteUser(id);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/check/{id}")
    public ResponseEntity checkUser(@PathVariable long id) {
        try {
            return new ResponseEntity<>(userControllerService.checkUser(id).getCategory(), HttpStatus.OK);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }
    }
}
