package com.aradosevic.eBookRepository.eBookRepository.controller;

import com.aradosevic.eBookRepository.eBookRepository.dto.query.QueryDTO;
import com.aradosevic.eBookRepository.eBookRepository.service.controller.QueryControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/queries")
public class QueryController {

    @Autowired
    private QueryControllerService queryControllerService;

    @PutMapping(value = "/query")
    public ResponseEntity getBooksByQuery(@RequestBody QueryDTO queryDTO) {
        return new ResponseEntity<>(queryControllerService.getAllByQuery(queryDTO), HttpStatus.OK);
    }

    @GetMapping(value = "/category/{id}")
    public ResponseEntity getBooksByCategory(@PathVariable long id) {
        return new ResponseEntity<>(queryControllerService.getByCategoryId(id), HttpStatus.OK);
    }
}
