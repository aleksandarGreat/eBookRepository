package com.aradosevic.eBookRepository.eBookRepository.controller;

import com.aradosevic.eBookRepository.eBookRepository.model.User;
import com.aradosevic.eBookRepository.eBookRepository.repository.elasticsearch.UserRepository;
import com.aradosevic.eBookRepository.eBookRepository.service.repository.UserService;
import org.elasticsearch.common.unit.Fuzziness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.MatchQueryBuilder.Operator.AND;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@RestController
@RequestMapping(value = "/api/es")
public class ElasticController {

    @Autowired
    private UserService userService;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @GetMapping(value = "/all")
    public ResponseEntity getAllUsers() {
        List<User> users = new ArrayList<>();
        userService.findAll().forEach(users::add);
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping(value = "/fuzzy")
    public ResponseEntity getFuzzy() {
        String userName = "Aleksan";
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(matchQuery("firstName", userName).minimumShouldMatch("50%"))
                .build();

        SearchQuery searchQuery1 = new NativeSearchQueryBuilder()
                .withQuery(matchQuery("firstName", userName)
                        .operator(AND)
                        .fuzziness(Fuzziness.ONE)
                        .prefixLength(3))
                .build();

        String userName1 = "Alelsandar";
        SearchQuery searchQuery2 = new NativeSearchQueryBuilder()
                .withQuery(matchQuery("firstName", userName1)
                        .operator(AND)
                        .fuzziness(Fuzziness.ONE)
                        .prefixLength(3))
                .build();

        List<User> users = elasticsearchTemplate.queryForList(searchQuery1, User.class);
        List<User> users2 = elasticsearchTemplate.queryForList(searchQuery2, User.class);
//        userService.findAll().forEach(users::add);
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
}
