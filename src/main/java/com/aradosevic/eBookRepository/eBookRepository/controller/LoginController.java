package com.aradosevic.eBookRepository.eBookRepository.controller;

import com.aradosevic.eBookRepository.eBookRepository.dto.login.LoginDTO;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomException;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomExceptionHandler;
import com.aradosevic.eBookRepository.eBookRepository.service.controller.LoginControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class LoginController {

    @Autowired
    private LoginControllerService loginControllerService;

    @PostMapping(value = "/login")
    public ResponseEntity login(@RequestBody LoginDTO loginDTO) {
        try {
            return new ResponseEntity<>(loginControllerService.login(loginDTO), HttpStatus.OK);
        } catch (CustomException e) {
            return CustomExceptionHandler.handleException(e, HttpStatus.BAD_REQUEST);
        }
    }
}
