package com.aradosevic.eBookRepository.eBookRepository.loader;

import com.aradosevic.eBookRepository.eBookRepository.model.*;
import com.aradosevic.eBookRepository.eBookRepository.repository.elasticsearch.BookRepository;
import com.aradosevic.eBookRepository.eBookRepository.repository.elasticsearch.UserRepository;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.BookJPARepository;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.CategoryJPARepository;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.LanguageJPARepository;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.UserJPARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataLoader {

    @Autowired
    private ElasticsearchOperations elasticsearchOperations;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserJPARepository userJPARepository;

    @Autowired
    private CategoryJPARepository categoryJPARepository;

    @Autowired
    private LanguageJPARepository languageJPARepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookJPARepository bookJPARepository;

    @PostConstruct
    @Transactional
    public void initializeDatabas() {
        insertUsers();
        insertCategories();
        insertLanguages();

        //updateBooks();
    }

    private void insertUsers() {
        if (userJPARepository.findAll().isEmpty()) {
            userJPARepository.save(createUsers());
        }
    }

    private List<User> createUsers() {
        List<User> users = new ArrayList<>();
        users.add(new User( "admin", "admin", "admin", "admin", User.UserType.ADMINISTRATOR));
        users.add(new User( "Aleksandar", "Radosevic", "alek", "123", User.UserType.SUBSCRIBER));
        return users;
    }

    private void insertCategories() {
        if (categoryJPARepository.findAll().isEmpty()) {
            categoryJPARepository.save(createCategories());
        }
    }

    private List<Category> createCategories() {
        List<Category> categories = new ArrayList<>();
        categories.add(new Category("Java"));
        categories.add(new Category("iOS"));
        categories.add(new Category("Spring Boot"));
        return categories;
    }

    private void insertLanguages() {
        if (languageJPARepository.findAll().isEmpty()) {
            languageJPARepository.save(createLanguages());
        }
    }

    private List<Language> createLanguages() {
        List<Language> languages = new ArrayList<>();
        languages.add(new Language("Serbian"));
        languages.add(new Language("English"));
        languages.add(new Language("German"));
        return languages;
    }

    private void updateBooks() {
        elasticsearchOperations.deleteIndex(Book.class);
        elasticsearchOperations.createIndex(Book.class);
        elasticsearchOperations.putMapping(Book.class);
        List<Book> books = bookJPARepository.findAll();
        if (!books.isEmpty()) {
            bookRepository.save(books);
        }
    }
}
