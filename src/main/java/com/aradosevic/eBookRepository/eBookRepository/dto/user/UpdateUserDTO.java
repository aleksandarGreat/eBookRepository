package com.aradosevic.eBookRepository.eBookRepository.dto.user;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class UpdateUserDTO {

    @NotNull
    private long id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    private String newUsername;

    public UpdateUserDTO() {
    }

    public UpdateUserDTO(long id, String firstName, String lastName, String newUsername) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.newUsername = newUsername;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }

    @Override
    public String toString() {
        return "UpdateUserDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", newUsername='" + newUsername + '\'' +
                '}';
    }
}
