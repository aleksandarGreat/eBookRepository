package com.aradosevic.eBookRepository.eBookRepository.dto.book;

import com.aradosevic.eBookRepository.eBookRepository.dto.category.CategoryDTO;
import com.aradosevic.eBookRepository.eBookRepository.dto.language.LanguageDTO;
import com.aradosevic.eBookRepository.eBookRepository.model.Book;
import com.aradosevic.eBookRepository.eBookRepository.model.Category;
import com.aradosevic.eBookRepository.eBookRepository.model.Language;

public class BookDTO {

    private long id;

    private String title;

    private String author;

    private String keywords;

    private long publicationYear;

    private String MIME;

    private CategoryDTO category;

    private LanguageDTO language;

    private String content;

    public BookDTO() {
    }

    public BookDTO(long id, String title, String author, String keywords, long publicationYear, String MIME, Category category, Language language, String content) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.keywords = keywords;
        this.publicationYear = publicationYear;
        this.MIME = MIME;
        this.category = new CategoryDTO(category);
        this.language = new LanguageDTO(language);
        this.content = content;
    }

    public BookDTO(Book book) {
        this.id = book.getId();
        this.title = book.getTitle();
        this.author = book.getAuthor();
        this.keywords = book.getKeywords();
        this.publicationYear = book.getPublicationYear();
        this.MIME = book.getMIME();
        this.category = new CategoryDTO(book.getCategory());
        this.language = new LanguageDTO(book.getLanguage());
        this.content = book.getContent();
    }

    public BookDTO(Book book, String content) {
        this.id = book.getId();
        this.title = book.getTitle();
        this.author = book.getAuthor();
        this.keywords = book.getKeywords();
        this.publicationYear = book.getPublicationYear();
        this.MIME = book.getMIME();
        this.category = new CategoryDTO(book.getCategory());
        this.language = new LanguageDTO(book.getLanguage());
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public long getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(long publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getMIME() {
        return MIME;
    }

    public void setMIME(String MIME) {
        this.MIME = MIME;
    }

    public CategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CategoryDTO category) {
        this.category = category;
    }

    public LanguageDTO getLanguage() {
        return language;
    }

    public void setLanguage(LanguageDTO language) {
        this.language = language;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "BookDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", keywords='" + keywords + '\'' +
                ", publicationYear=" + publicationYear +
                ", MIME='" + MIME + '\'' +
                ", category='" + category + '\'' +
                ", language='" + language + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
