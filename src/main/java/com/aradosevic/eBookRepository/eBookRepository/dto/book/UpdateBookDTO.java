package com.aradosevic.eBookRepository.eBookRepository.dto.book;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class UpdateBookDTO {

    @NotNull
    private long id;

    @NotBlank
    private String title;

    @NotBlank
    private String author;

    @NotBlank
    private String keywords;

    @NotNull
    private long publicationYear;

    @NotBlank
    private String MIME;

    @NotNull
    private long categoryId;

    @NotNull
    private long languageId;

    public UpdateBookDTO() {
    }

    public UpdateBookDTO(long id, String title, String author, String keywords, long publicationYear, String MIME, long categoryId, long languageId) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.keywords = keywords;
        this.publicationYear = publicationYear;
        this.MIME = MIME;
        this.categoryId = categoryId;
        this.languageId = languageId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public long getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(long publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getMIME() {
        return MIME;
    }

    public void setMIME(String MIME) {
        this.MIME = MIME;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    @Override
    public String toString() {
        return "UpdateBookDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", keywords='" + keywords + '\'' +
                ", publicationYear=" + publicationYear +
                ", MIME='" + MIME + '\'' +
                ", categoryId=" + categoryId +
                ", languageId=" + languageId +
                '}';
    }
}
