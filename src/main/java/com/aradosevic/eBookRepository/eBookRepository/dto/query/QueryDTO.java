package com.aradosevic.eBookRepository.eBookRepository.dto.query;

public class QueryDTO {

    private String title;

    private String author;

    private String keywords;

    private Long publicationYear;

    private Long categoryId;

    private Long languageId;

    private String content;

    public QueryDTO() {
    }

    public QueryDTO(String title, String author, String keywords, Long publicationYear, Long categoryId, Long languageId, String content) {
        this.title = title;
        this.author = author;
        this.keywords = keywords;
        this.publicationYear = publicationYear;
        this.categoryId = categoryId;
        this.languageId = languageId;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Long getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(Long publicationYear) {
        this.publicationYear = publicationYear;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "QueryDTO{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", keywords='" + keywords + '\'' +
                ", publicationYear=" + publicationYear +
                ", categoryId=" + categoryId +
                ", languageId=" + languageId +
                ", content='" + content + '\'' +
                '}';
    }
}
