package com.aradosevic.eBookRepository.eBookRepository.dto.user;

import com.aradosevic.eBookRepository.eBookRepository.dto.category.CategoryDTO;
import com.aradosevic.eBookRepository.eBookRepository.model.User;

public class UserDTO {

    private long id;

    private String firstName;

    private String lastName;

    private String username;

    private String type;

    private CategoryDTO category;

    public UserDTO() {
    }

    public UserDTO(long id, String firstName, String lastName, String username, String type, CategoryDTO category) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.type = type;
        this.category = category;
    }

    public UserDTO(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.username = user.getUsername();
        this.type = user.getType().toString();
        if (user.getCategory() != null) {
            this.category = new CategoryDTO(user.getCategory());
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CategoryDTO category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", type='" + type + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
