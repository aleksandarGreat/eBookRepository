package com.aradosevic.eBookRepository.eBookRepository.dto.book;

import com.aradosevic.eBookRepository.eBookRepository.model.Book;

public class BookCreatedDTO {

    private long id;

    private String title;

    private String author;

    private String keywords;

    private long publicationYear;

    private String MIME;

    private long categoryId;

    private long languageId;

    public BookCreatedDTO() {
    }

    public BookCreatedDTO(Book book) {
        this.id = book.getId();
        this.title = book.getTitle();
        this.author = book.getAuthor();
        this.keywords = book.getKeywords();
        this.publicationYear = book.getPublicationYear();
        this.MIME = book.getMIME();
        this.languageId = book.getLanguage().getId();
        this.categoryId = book.getCategory().getId();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public long getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(long publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getMIME() {
        return MIME;
    }

    public void setMIME(String MIME) {
        this.MIME = MIME;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    @Override
    public String toString() {
        return "BookCreatedDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", keywords='" + keywords + '\'' +
                ", publicationYear=" + publicationYear +
                ", MIME='" + MIME + '\'' +
                ", categoryId=" + categoryId +
                ", languageId=" + languageId +
                '}';
    }
}
