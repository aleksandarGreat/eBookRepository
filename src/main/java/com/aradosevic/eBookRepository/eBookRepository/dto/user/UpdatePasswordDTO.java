package com.aradosevic.eBookRepository.eBookRepository.dto.user;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class UpdatePasswordDTO {

    @NotNull
    private long id;

    @NotBlank
    private String oldPassword;

    @NotBlank
    private String newPassword;

    private String retypedPassword;

    public UpdatePasswordDTO() {
    }

    public UpdatePasswordDTO(long id, String oldPassword, String newPassword, String retypedPassword) {
        this.id = id;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.retypedPassword = retypedPassword;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUsername() {
        return id;
    }

    public void setUsername(long username) {
        this.id = username;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRetypedPassword() {
        return retypedPassword;
    }

    public void setRetypedPassword(String retypedPassword) {
        this.retypedPassword = retypedPassword;
    }

    @Override
    public String toString() {
        return "UpdatePasswordDTO{" +
                "id=" + id +
                ", oldPassword='" + oldPassword + '\'' +
                ", newPassword='" + newPassword + '\'' +
                ", retypedPassword='" + retypedPassword + '\'' +
                '}';
    }
}
