package com.aradosevic.eBookRepository.eBookRepository.dto.category;

public class UpdateCategoryDTO {

    private long id;

    private String name;

    public UpdateCategoryDTO() {
    }

    public UpdateCategoryDTO(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UpdateCategoryDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
