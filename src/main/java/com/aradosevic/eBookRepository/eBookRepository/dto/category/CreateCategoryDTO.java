package com.aradosevic.eBookRepository.eBookRepository.dto.category;

import org.hibernate.validator.constraints.NotBlank;

public class CreateCategoryDTO {

    @NotBlank
    private String name;

    public CreateCategoryDTO() {
    }

    public CreateCategoryDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CreateCategoryDTO{" +
                "name='" + name + '\'' +
                '}';
    }
}
