package com.aradosevic.eBookRepository.eBookRepository.dto.language;

import com.aradosevic.eBookRepository.eBookRepository.model.Language;

public class LanguageDTO {

    private long id;

    private String name;

    public LanguageDTO() {
    }

    public LanguageDTO(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public LanguageDTO(Language language) {
        this.id = language.getId();
        this.name = language.getName();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "LanguageDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
