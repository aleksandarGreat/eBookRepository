package com.aradosevic.eBookRepository.eBookRepository.dto.user;

import org.hibernate.validator.constraints.NotBlank;

public class UpdateUsernameDTO {

    @NotBlank
    private String oldUsername;

    @NotBlank
    private String newUsername;

    public UpdateUsernameDTO() {
    }

    public UpdateUsernameDTO(String oldUsername, String newUsername) {
        this.oldUsername = oldUsername;
        this.newUsername = newUsername;
    }

    public String getOldUsername() {
        return oldUsername;
    }

    public void setOldUsername(String oldUsername) {
        this.oldUsername = oldUsername;
    }

    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }

    @Override
    public String toString() {
        return "UpdateUsernameDTO{" +
                "oldUsername='" + oldUsername + '\'' +
                ", newUsername='" + newUsername + '\'' +
                '}';
    }
}
