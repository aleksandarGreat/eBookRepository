package com.aradosevic.eBookRepository.eBookRepository.service.controller;

import com.aradosevic.eBookRepository.eBookRepository.dto.category.CategoryDTO;
import com.aradosevic.eBookRepository.eBookRepository.dto.category.CreateCategoryDTO;
import com.aradosevic.eBookRepository.eBookRepository.dto.category.UpdateCategoryDTO;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomException;
import com.aradosevic.eBookRepository.eBookRepository.model.Category;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.CategoryJPARepository;
import com.aradosevic.eBookRepository.eBookRepository.service.repository.CategoryJPAWrapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryControllerService {

    @Autowired
    private CategoryJPARepository categoryJPARepository;

    @Autowired
    private CategoryJPAWrapperService categoryJPAWrapperService;

    public List<CategoryDTO> getAllCategories() {
        return categoryJPARepository.findAll().stream().map(CategoryDTO::new).collect(Collectors.toList());
    }

    public void createCategory(CreateCategoryDTO createCategoryDTO) throws CustomException {
        Category category = categoryJPAWrapperService.findByName(createCategoryDTO.getName());
        if (category != null) {
            throw new CustomException("Category with that name already exists!");
        }

        category = new Category(createCategoryDTO.getName());
        categoryJPARepository.save(category);
    }

    public void updateCategory(UpdateCategoryDTO updateCategoryDTO) throws CustomException {
        Category category = categoryJPARepository.findOne(updateCategoryDTO.getId());
        if (category == null) {
            throw new CustomException("No category with given id!");
        }

        Category checkNameCategory = categoryJPAWrapperService.findByName(updateCategoryDTO.getName());
        if (checkNameCategory != null) {
            throw new CustomException("Category with that name already exists!");
        }

        category.setName(updateCategoryDTO.getName());
        categoryJPARepository.save(category);
    }

    public void deleteCategory(long id) throws CustomException {
        Category category = categoryJPARepository.findOne(id);
        if (category == null) {
            throw new CustomException("Category with given id does not exist and cannot be deleted.");
        }

        categoryJPARepository.delete(category);
    }
}
