package com.aradosevic.eBookRepository.eBookRepository.service.controller;

import com.aradosevic.eBookRepository.eBookRepository.dto.user.*;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomException;
import com.aradosevic.eBookRepository.eBookRepository.model.Category;
import com.aradosevic.eBookRepository.eBookRepository.model.User;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.CategoryJPARepository;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.UserJPARepository;
import com.aradosevic.eBookRepository.eBookRepository.service.repository.UserJPAWrapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserControllerService {

    @Autowired
    private UserJPARepository userJPARepository;

    @Autowired
    private CategoryJPARepository categoryJPARepository;

    @Autowired
    private UserJPAWrapperService userJPAWrapperService;

    public List<UserDTO> getAllUsers() {
        return userJPARepository.findAll().stream().map(UserDTO::new).collect(Collectors.toList());
    }

    public void changeUser(ChangeUserDTO changeUserDTO) throws CustomException {
        User user = userJPARepository.findOne(changeUserDTO.getId());
        if (user == null) {
            throw new CustomException("User with given id does not exist!");
        }

        User checkNewUsername = userJPAWrapperService.findByUsername(changeUserDTO.getUsername());
        if (checkNewUsername != null && checkNewUsername.getId() != user.getId()) {
            throw new CustomException("User with desired new username already exists, try another one");
        }

        if (changeUserDTO.getCategoryId() != 0) {
            Category category = categoryJPARepository.findOne(changeUserDTO.getCategoryId());
            if (category == null) {
                throw new CustomException("Category with given id not found.");
            }
            user = new User(changeUserDTO, category, user);
            userJPARepository.save(user);
        } else {
            user = new User(changeUserDTO,user);
            userJPARepository.save(user);
        }
    }

    public void updateUser(UpdateUserDTO updateUserDTO) throws CustomException {
        User user = userJPARepository.findOne(updateUserDTO.getId());
        if (user == null) {
            throw new CustomException("User with given id does not exist!");
        }

        User checkNewUsername = userJPAWrapperService.findByUsername(updateUserDTO.getNewUsername());
        if (checkNewUsername != null && checkNewUsername.getId() != user.getId()) {
            throw new CustomException("User with desired new username already exists, try another one");
        }

        user.setUsername(updateUserDTO.getNewUsername());
        user.setFirstName(updateUserDTO.getFirstName());
        user.setLastName(updateUserDTO.getLastName());
        userJPARepository.save(user);
    }

    public void updatePassword(UpdatePasswordDTO updatePasswordDTO) throws CustomException {
        User user = userJPARepository.findOne(updatePasswordDTO.getId());
        if (user == null) {
            throw new CustomException("User with given id does not exist!");
        }

        if (!user.getPassword().equals(updatePasswordDTO.getOldPassword())) {
            throw new CustomException("Not correct old password.");
        }

        if (!updatePasswordDTO.getNewPassword().equals(updatePasswordDTO.getRetypedPassword())) {
            throw new CustomException("Passwords do not match!");
        }

        user.setPassword(updatePasswordDTO.getNewPassword());
        userJPARepository.save(user);
    }

    public void createUser(CreateUserDTO createUserDTO) throws CustomException {
        User user = userJPAWrapperService.findByUsername(createUserDTO.getUsername());
        if (user != null) {
            throw new CustomException("Username is taken, please try different one.");
        }

        if (createUserDTO.getCategoryId() != 0) {
            Category category = categoryJPARepository.findOne(createUserDTO.getCategoryId());
            if (category == null) {
                throw new CustomException("Category with given id not found.");
            }
            user = new User(createUserDTO, category);
            userJPARepository.save(user);
        } else {
            user = new User(createUserDTO);
            userJPARepository.save(user);
        }
    }

    public void deleteUser(long id) throws CustomException {
        User user = userJPARepository.findOne(id);
        if (user == null) {
            throw new CustomException("User with given id doest not exist and cannot be deleted!");
        }

        userJPARepository.delete(id);
    }

    public User checkUser(long id) throws CustomException {
        User user = userJPARepository.findOne(id);
        if (user == null) {
            throw new CustomException("User with given id does not exist!");
        }

        return user;
    }
}
