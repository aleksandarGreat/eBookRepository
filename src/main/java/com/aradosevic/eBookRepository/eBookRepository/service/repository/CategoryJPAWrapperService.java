package com.aradosevic.eBookRepository.eBookRepository.service.repository;

import com.aradosevic.eBookRepository.eBookRepository.model.Category;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.CategoryJPARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryJPAWrapperService {

    @Autowired
    private CategoryJPARepository categoryJPARepository;

    public Category findByName(String name) {
        List<Category> categories =  categoryJPARepository.findByName(name);
        if (!categories.isEmpty()) {
            return categories.get(0);
        }

        return null;
    }
}
