package com.aradosevic.eBookRepository.eBookRepository.service.repository;

import com.aradosevic.eBookRepository.eBookRepository.model.User;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.UserJPARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserJPAWrapperService {

    @Autowired
    private UserJPARepository userJPARepository;

    public User findByUsername(String username) {
        List<User> users = userJPARepository.findByUsername(username);
        if (!users.isEmpty()) {
            return users.get(0);
        }
        return null;
    }

    public User findByUsernameAndPassword(String username, String password) {
        List<User> users = userJPARepository.findByUsernameAndPassword(username, password);
        if (!users.isEmpty()) {
            return users.get(0);
        }

        return null;
    }
}
