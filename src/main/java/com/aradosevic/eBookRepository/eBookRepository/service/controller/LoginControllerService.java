package com.aradosevic.eBookRepository.eBookRepository.service.controller;

import com.aradosevic.eBookRepository.eBookRepository.dto.login.LoginDTO;
import com.aradosevic.eBookRepository.eBookRepository.dto.user.UserDTO;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomException;
import com.aradosevic.eBookRepository.eBookRepository.model.User;
import com.aradosevic.eBookRepository.eBookRepository.service.repository.UserJPAWrapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginControllerService {

    @Autowired
    private UserJPAWrapperService userJPAWrapperService;

    public UserDTO login(LoginDTO loginDTO) throws CustomException {
        User user = userJPAWrapperService.findByUsernameAndPassword(loginDTO.getUsername(), loginDTO.getPassword());
        if (user == null) {
            throw new CustomException("Wrong username or password");
        }

        return new UserDTO(user);
    }
}
