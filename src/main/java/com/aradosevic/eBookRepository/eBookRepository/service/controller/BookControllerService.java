package com.aradosevic.eBookRepository.eBookRepository.service.controller;

import com.aradosevic.eBookRepository.eBookRepository.dto.book.BookCreatedDTO;
import com.aradosevic.eBookRepository.eBookRepository.dto.book.UpdateBookDTO;
import com.aradosevic.eBookRepository.eBookRepository.exception.CustomException;
import com.aradosevic.eBookRepository.eBookRepository.model.Book;
import com.aradosevic.eBookRepository.eBookRepository.model.Category;
import com.aradosevic.eBookRepository.eBookRepository.model.Language;
import com.aradosevic.eBookRepository.eBookRepository.repository.elasticsearch.BookRepository;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.BookJPARepository;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.CategoryJPARepository;
import com.aradosevic.eBookRepository.eBookRepository.repository.jpa.LanguageJPARepository;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

@Service
public class BookControllerService {

    private static String BOOK_DIR = "books";

    private String categoryPath;

    private Category category;

    private Language language;

    @Autowired
    private CategoryJPARepository categoryJPARepository;

    @Autowired
    private LanguageJPARepository languageJPARepository;

    @Autowired
    private BookJPARepository bookJPARepository;

    @Autowired
    private BookRepository bookRepository;

    public BookCreatedDTO uploadBook(MultipartFile file, long categoryId, long languageId) throws CustomException {
        category = categoryJPARepository.findOne(categoryId);
        if (category == null) {
            throw new CustomException("No category with given id!");
        }

        language = languageJPARepository.findOne(languageId);
        if (language == null) {
            throw new CustomException("No language with given id!");
        }

        createBooksDirectory();
        return saveBook(file);
    }

    public void updateBook(UpdateBookDTO updateBookDTO) throws CustomException {
        Book book = bookRepository.findOne(updateBookDTO.getId());
        if (book == null) {
            throw new CustomException("Book with given id not found!");
        }

        book = new Book(updateBookDTO, book);

        Category category = categoryJPARepository.findOne(updateBookDTO.getCategoryId());
        if (category == null) {
            throw new CustomException("Category with given id not found!");
        }

        Language language = languageJPARepository.findOne(updateBookDTO.getLanguageId());
        if (language == null) {
            throw new CustomException("Language with given id not found!");
        }

        book.setCategory(category);
        book.setLanguage(language);
        bookRepository.save(book);
        book.setContent(null);
        bookJPARepository.save(book);
    }

    public Book downloadBook(long id) throws CustomException {
        Book book = bookJPARepository.findOne(id);
        if (book == null) {
            throw new CustomException("Book with given id not found!");
        }

        return book;
    }

    public void deleteBook(long id) throws CustomException {
        Book book = bookJPARepository.findOne(id);
        if (book == null) {
            throw new CustomException("Book with given id not found and cannot be deleted!");
        }

        File file = new File(book.getFilePath());
        if (!file.delete()) {
            throw new CustomException("Could not delete file, does not exist!");
        }

        bookRepository.delete(book);
        bookJPARepository.delete(book);
    }

    private void createBooksDirectory() throws CustomException {
        File booksDirectory = new File(BOOK_DIR);
        File categoryDirectory = new File(BOOK_DIR + "/"+ category.getName());
        categoryPath = categoryDirectory.getAbsolutePath() + "/";

        createDirectory(booksDirectory);
        createDirectory(categoryDirectory);
    }

    private void createDirectory(File directory) throws CustomException {
        if (!directory.exists()) {
            try{
               if (!directory.mkdir()) {
                   throw new CustomException("Directory could not be created, check permissions.");
               }
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
        }
    }

    private BookCreatedDTO saveBook(MultipartFile file) throws CustomException {
        if (file.isEmpty()) {
            throw new CustomException("Please select file to upload");
        }

        String filePath;
        String content;
        PDDocumentInformation pdDocumentInformation;
        try {
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();

            filePath = categoryPath + file.getOriginalFilename().split("\\.pdf")[0] + "_" + new Date().getTime() + ".pdf";
            filePath = filePath.replaceAll("[^\\p{ASCII}]", "");
            Path path = Paths.get(filePath);
            Files.write(path, bytes);
            PDDocument document = PDDocument.load(new File(filePath));
            content = new PDFTextStripper().getText(document);
            pdDocumentInformation = document.getDocumentInformation();
        } catch (IOException e) {
            e.printStackTrace();
            throw new CustomException(Arrays.toString(e.getStackTrace()));
        }

        return saveBookInDatabase(filePath, content, pdDocumentInformation, file.getOriginalFilename().split("\\.pdf")[0].replaceAll("[^\\p{ASCII}]", ""));
    }

    private BookCreatedDTO saveBookInDatabase(String filePath, String content, PDDocumentInformation info, String name) {
        Book book = new Book();
        book.setCategory(category);
        book.setLanguage(language);
        book.setTitle(info.getTitle());
        book.setAuthor(info.getAuthor());
        book.setKeywords(info.getKeywords());
        if (info.getCreationDate() != null) {
            book.setPublicationYear(info.getCreationDate().get(Calendar.YEAR));
        }
        book.setFilePath(filePath);
        book.setName(name);
        book = bookJPARepository.save(book);
        book.setContent(content);
        bookRepository.save(book);
        return new BookCreatedDTO(book);
    }
}
