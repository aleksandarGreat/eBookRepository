package com.aradosevic.eBookRepository.eBookRepository.service.controller;

import com.aradosevic.eBookRepository.eBookRepository.dto.book.BookDTO;
import com.aradosevic.eBookRepository.eBookRepository.dto.query.QueryDTO;
import com.aradosevic.eBookRepository.eBookRepository.model.Book;
import com.aradosevic.eBookRepository.eBookRepository.repository.elasticsearch.BookRepository;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.highlight.HighlightBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.matchPhraseQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@Service
public class QueryControllerService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    public List<BookDTO> getAllByQuery(QueryDTO queryDTO) {

        if (checkIfAllAreNull(queryDTO)) {
            return returnAll();
        }

        return search(queryDTO);
    }

    public List<BookDTO> getByCategoryId(long id) {
        return bookRepository.findByCategoryId(id).stream().map(BookDTO::new).collect(Collectors.toList());
    }

    private boolean checkIfAllAreNull(QueryDTO queryDTO) {

        return StringUtils.isEmpty(queryDTO.getTitle()) &&
                StringUtils.isEmpty(queryDTO.getAuthor()) &&
                StringUtils.isEmpty(queryDTO.getKeywords()) &&
                queryDTO.getPublicationYear() == null &&
                queryDTO.getCategoryId() == null &&
                queryDTO.getLanguageId() == null &&
                StringUtils.isEmpty(queryDTO.getContent());
    }

    private List<BookDTO> returnAll() {
        List<Book> books = new ArrayList<>();
        bookRepository.findAll().forEach(books::add);
        List<BookDTO> bookDTOs = new ArrayList<>();
        for (Book book : books) {
            bookDTOs.add(new BookDTO(book, null));
        }
        return bookDTOs;
    }

    private void prepareBoolQueries(QueryDTO queryDTO, BoolQueryBuilder nativeBooleanQueryBuilder,
                                    BoolQueryBuilder phraseBooleanQueryBuilder, BoolQueryBuilder fuzzyBooleanQueryBuilder,
                                    BoolQueryBuilder phraseSlopBooleanQueryBuilder) {
        if (!StringUtils.isEmpty(queryDTO.getTitle())) {
            nativeBooleanQueryBuilder.must(matchQuery("title", queryDTO.getTitle()));
            fuzzyBooleanQueryBuilder.should(matchQuery("title", queryDTO.getTitle())
                    .fuzziness(Fuzziness.ONE)
                    .prefixLength(2));
            if (queryDTO.getTitle().split(" ").length > 1) {
                phraseBooleanQueryBuilder.must(matchPhraseQuery("title", queryDTO.getTitle()).slop(1));
                phraseBooleanQueryBuilder.must(matchPhraseQuery("title", queryDTO.getTitle()).slop(3));

            }
        }

        if (!StringUtils.isEmpty(queryDTO.getAuthor())) {
            nativeBooleanQueryBuilder.must(matchQuery("author", queryDTO.getAuthor()));
            fuzzyBooleanQueryBuilder.should(matchQuery("author", queryDTO.getAuthor())
                    .fuzziness(Fuzziness.ONE)
                    .prefixLength(2));
            if (queryDTO.getAuthor().split(" ").length > 1) {
                phraseBooleanQueryBuilder.must(matchPhraseQuery("author", queryDTO.getAuthor()).slop(1));
                phraseBooleanQueryBuilder.must(matchPhraseQuery("author", queryDTO.getAuthor()).slop(3));

            }
        }

        if (!StringUtils.isEmpty(queryDTO.getKeywords())) {
            nativeBooleanQueryBuilder.must(matchQuery("keywords", queryDTO.getKeywords()));
            fuzzyBooleanQueryBuilder.should(matchQuery("keywords", queryDTO.getKeywords())
                    .fuzziness(Fuzziness.ONE)
                    .prefixLength(2));
            if (queryDTO.getKeywords().split(",").length > 1) {
                phraseBooleanQueryBuilder.must(matchPhraseQuery("keywords", queryDTO.getKeywords()).slop(1));
                phraseBooleanQueryBuilder.must(matchPhraseQuery("keywords", queryDTO.getKeywords()).slop(3));

            }
        }

        if (queryDTO.getPublicationYear() != null) {
            nativeBooleanQueryBuilder.must(matchQuery("publicationYear", queryDTO.getPublicationYear()));
            fuzzyBooleanQueryBuilder.should(matchQuery("publicationYear", queryDTO.getPublicationYear())
                    .fuzziness(Fuzziness.ONE)
                    .prefixLength(2));
            phraseBooleanQueryBuilder.must(matchPhraseQuery("publicationYear", queryDTO.getPublicationYear()));
        }

        if (queryDTO.getCategoryId() != null) {
            nativeBooleanQueryBuilder.must(matchQuery("category.id", queryDTO.getCategoryId()));
            fuzzyBooleanQueryBuilder.should(matchQuery("category.id", queryDTO.getCategoryId())
                    .fuzziness(Fuzziness.ONE)
                    .prefixLength(2));
            phraseBooleanQueryBuilder.must(matchPhraseQuery("category.id", queryDTO.getCategoryId()));
        }

        if (queryDTO.getLanguageId() != null) {
            nativeBooleanQueryBuilder.must(matchQuery("language.id", queryDTO.getLanguageId()));
            fuzzyBooleanQueryBuilder.should(matchQuery("language.id", queryDTO.getLanguageId())
                    .fuzziness(Fuzziness.ONE)
                    .prefixLength(2));
            phraseBooleanQueryBuilder.must(matchPhraseQuery("language.id", queryDTO.getLanguageId()));
        }

        if (!StringUtils.isEmpty(queryDTO.getContent())) {
            nativeBooleanQueryBuilder.should(matchQuery("content", queryDTO.getContent()));
            fuzzyBooleanQueryBuilder.should(matchQuery("content", queryDTO.getContent())
                    .fuzziness(Fuzziness.ONE)
                    .prefixLength(2));
            if (queryDTO.getContent().split(" ").length > 1) {
                phraseBooleanQueryBuilder.must(matchPhraseQuery("keywords", queryDTO.getKeywords()).slop(1));
                phraseBooleanQueryBuilder.must(matchPhraseQuery("keywords", queryDTO.getKeywords()).slop(3));

            }
        }
    }

    private List<BookDTO> search(QueryDTO queryDTO) {
        BoolQueryBuilder nativeBoolQueryBuilder = new BoolQueryBuilder();
        BoolQueryBuilder fuzzyBooleanQueryBuilder = new BoolQueryBuilder();
        BoolQueryBuilder phraseBooleanQueryBuilder = new BoolQueryBuilder();
        BoolQueryBuilder phraseSlopBooleanQueryBuilder = new BoolQueryBuilder();
        NativeSearchQueryBuilder nativeSearchQueryBuilder1 = new NativeSearchQueryBuilder();

        prepareBoolQueries(queryDTO, nativeBoolQueryBuilder, phraseBooleanQueryBuilder, fuzzyBooleanQueryBuilder, phraseSlopBooleanQueryBuilder);

        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder()
                .withQuery(nativeBoolQueryBuilder);

        if (queryDTO.getContent() != null) {
            nativeSearchQueryBuilder.withHighlightFields(new HighlightBuilder.Field("content"));
        }

        SearchQuery searchQuery = nativeSearchQueryBuilder.build();

        final List<Book> books = elasticsearchTemplate.queryForList(searchQuery, Book.class);

        if (books.isEmpty()) {
            nativeSearchQueryBuilder = new NativeSearchQueryBuilder().withQuery(phraseBooleanQueryBuilder);
            if (queryDTO.getContent() != null) {
                nativeSearchQueryBuilder.withHighlightFields(new HighlightBuilder.Field("content"));
            }

            searchQuery = nativeSearchQueryBuilder.build();

            final List<Book> phraseBooks = elasticsearchTemplate.queryForList(searchQuery, Book.class);

            if (mapBooks(queryDTO, searchQuery, phraseBooks))
                return phraseBooks.stream().map(BookDTO::new).collect(Collectors.toList());

            nativeSearchQueryBuilder = new NativeSearchQueryBuilder().withQuery(phraseSlopBooleanQueryBuilder);
            if (queryDTO.getContent() != null) {
                nativeSearchQueryBuilder.withHighlightFields(new HighlightBuilder.Field("content"));
            }

            searchQuery = nativeSearchQueryBuilder.build();

            final List<Book> phraseSlopBooks = elasticsearchTemplate.queryForList(searchQuery, Book.class);

            if (mapBooks(queryDTO, searchQuery, phraseSlopBooks))
                return phraseSlopBooks.stream().map(BookDTO::new).collect(Collectors.toList());

            nativeSearchQueryBuilder = new NativeSearchQueryBuilder().withQuery(fuzzyBooleanQueryBuilder);
            if (queryDTO.getContent() != null) {
                nativeSearchQueryBuilder.withHighlightFields(new HighlightBuilder.Field("content"));
            }

            searchQuery = nativeSearchQueryBuilder.build();

            final List<Book> fuzzyBooks = elasticsearchTemplate.queryForList(searchQuery, Book.class);

            if (!StringUtils.isEmpty(queryDTO.getContent())) {
                mapHighlightedContent(searchQuery, fuzzyBooks);
            } else {
                fuzzyBooks.forEach(book -> book.setContent(null));
            }

            return fuzzyBooks.stream().map(BookDTO::new).collect(Collectors.toList());
        }

        if (!StringUtils.isEmpty(queryDTO.getContent())) {
            mapHighlightedContent(searchQuery, books);
        } else {
            books.forEach(book -> book.setContent(null));
        }

        return books.stream().map(BookDTO::new).collect(Collectors.toList());
    }

    private boolean mapBooks(QueryDTO queryDTO, SearchQuery searchQuery, List<Book> phraseSlopBooks) {
        if (!phraseSlopBooks.isEmpty()) {
            if (!StringUtils.isEmpty(queryDTO.getContent())) {
                mapHighlightedContent(searchQuery, phraseSlopBooks);
            } else {
                phraseSlopBooks.forEach(book -> book.setContent(null));
            }

            return true;
        }
        return false;
    }

    private void mapHighlightedContent(SearchQuery searchQuery, List<Book> books) {
        elasticsearchTemplate.queryForPage(searchQuery, Book.class, new SearchResultMapper() {

            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse searchResponse, Class<T> aClass, Pageable pageable) {
                for (int i = 0; i < searchResponse.getHits().totalHits(); i++) {
                    if (searchResponse.getHits().getHits().length != 0 &&
                        searchResponse.getHits().getAt(i).getHighlightFields().get("content") != null) {
                            books.get(i).setContent(searchResponse.getHits().getAt(i).getHighlightFields().get("content").toString());
                    } else {
                        books.get(i).setContent("");
                    }
                }
                return null;
            }
        });
    }
}
