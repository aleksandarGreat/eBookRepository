package com.aradosevic.eBookRepository.eBookRepository.service.repository;

import com.aradosevic.eBookRepository.eBookRepository.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface UserService {

    User save(User book);

    Iterable<User> findAll();

    Page<User> findByUsername(String username, PageRequest pageRequest);

    List<User> findByFirstName(String firstName);
}
