package com.aradosevic.eBookRepository.eBookRepository.model;

import com.aradosevic.eBookRepository.eBookRepository.dto.book.UpdateBookDTO;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Document(indexName = "book", type = "book", shards = 1)
public class Book {

    @Id
    @GeneratedValue
    private long id;

    private String title;

    private String author;

    private String keywords;

    private long publicationYear;

    private String filePath;

    private String MIME;

    private String name;

    private String content;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "language_id")
    private Language language;

    public Book() {
    }

    public Book(String title, String author, String keywords, long publicationYear, String filePath, String MIME, String name, String content, Category category, Language language) {
        this.title = title;
        this.author = author;
        this.keywords = keywords;
        this.publicationYear = publicationYear;
        this.filePath = filePath;
        this.MIME = MIME;
        this.name = name;
        this.content = content;
        this.category = category;
        this.language = language;
    }

    public Book(UpdateBookDTO updateBookDTO, Book book) {
        this.id = updateBookDTO.getId();
        this.title = updateBookDTO.getTitle();
        this.author = updateBookDTO.getAuthor();
        this.keywords = updateBookDTO.getKeywords();
        this.publicationYear = updateBookDTO.getPublicationYear();
        this.MIME = updateBookDTO.getMIME();
        this.filePath = book.getFilePath();
        this.name = book.getName();
        this.content = book.getContent();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public long getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(long publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMIME() {
        return MIME;
    }

    public void setMIME(String MIME) {
        this.MIME = MIME;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", keywords='" + keywords + '\'' +
                ", publicationYear=" + publicationYear +
                ", filePath='" + filePath + '\'' +
                ", MIME='" + MIME + '\'' +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", category=" + category +
                ", language=" + language +
                '}';
    }
}
