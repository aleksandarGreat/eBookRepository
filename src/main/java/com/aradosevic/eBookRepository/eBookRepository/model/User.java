package com.aradosevic.eBookRepository.eBookRepository.model;

import com.aradosevic.eBookRepository.eBookRepository.dto.user.ChangeUserDTO;
import com.aradosevic.eBookRepository.eBookRepository.dto.user.CreateUserDTO;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Document(indexName = "ebook", type = "user", shards = 1)
public class User implements Serializable {

    public enum UserType {
        ADMINISTRATOR,
        SUBSCRIBER
    }

    @Id
    @GeneratedValue
    private long id;

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private UserType type;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    public User() {
    }

    public User(long id, String firstName, String lastName, String username, String password, UserType type) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public User(String firstName, String lastName, String username, String password, UserType type) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public User(CreateUserDTO createUserDTO, Category category) {
        this.username = createUserDTO.getUsername();
        this.firstName = createUserDTO.getFirstName();
        this.lastName = createUserDTO.getLastName();
        this.password = createUserDTO.getPassword();
        this.type = createUserDTO.getType();
        this.category = category;
    }

    public User(CreateUserDTO createUserDTO) {
        this.username = createUserDTO.getUsername();
        this.firstName = createUserDTO.getFirstName();
        this.lastName = createUserDTO.getLastName();
        this.password = createUserDTO.getPassword();
        this.type = createUserDTO.getType();
    }

    public User(ChangeUserDTO changeUserDTO, Category category, User user) {
        this.id = changeUserDTO.getId();
        this.username = changeUserDTO.getUsername();
        this.firstName = changeUserDTO.getFirstName();
        this.lastName = changeUserDTO.getLastName();
        if (!StringUtils.isEmpty(changeUserDTO.getPassword())) {
            this.password = changeUserDTO.getPassword();
        } else {
            this.password = user.getPassword();
        }
        this.type = changeUserDTO.getType();
        this.category = category;
    }

    public User(ChangeUserDTO changeUserDTO, User user) {
        this.id = changeUserDTO.getId();
        this.username = changeUserDTO.getUsername();
        this.firstName = changeUserDTO.getFirstName();
        this.lastName = changeUserDTO.getLastName();
        if (!StringUtils.isEmpty(changeUserDTO.getPassword())) {
            this.password = changeUserDTO.getPassword();
        } else {
            this.password = user.getPassword();
        }
        this.type = changeUserDTO.getType();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", type=" + type +
                ", category=" + category +
                '}';
    }
}
