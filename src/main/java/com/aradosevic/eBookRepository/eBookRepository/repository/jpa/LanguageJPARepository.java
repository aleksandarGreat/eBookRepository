package com.aradosevic.eBookRepository.eBookRepository.repository.jpa;

import com.aradosevic.eBookRepository.eBookRepository.model.Language;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageJPARepository extends JpaRepository<Language, Long> {
}
