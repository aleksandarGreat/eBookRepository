package com.aradosevic.eBookRepository.eBookRepository.repository.elasticsearch;

import com.aradosevic.eBookRepository.eBookRepository.model.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface BookRepository extends ElasticsearchRepository<Book, Long> {

    List<Book> findByCategoryId(long id);
}
