package com.aradosevic.eBookRepository.eBookRepository.repository.jpa;

import com.aradosevic.eBookRepository.eBookRepository.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookJPARepository extends JpaRepository<Book, Long> {
}
