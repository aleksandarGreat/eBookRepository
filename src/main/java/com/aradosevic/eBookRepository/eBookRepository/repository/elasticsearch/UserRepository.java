package com.aradosevic.eBookRepository.eBookRepository.repository.elasticsearch;

import com.aradosevic.eBookRepository.eBookRepository.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends ElasticsearchRepository<User, Long> {

    Page<User> findByUsername(String username, Pageable pageable);

    List<User> findByFirstName(String firstName);
}
