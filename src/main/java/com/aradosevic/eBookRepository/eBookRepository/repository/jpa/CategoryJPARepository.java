package com.aradosevic.eBookRepository.eBookRepository.repository.jpa;

import com.aradosevic.eBookRepository.eBookRepository.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryJPARepository extends JpaRepository<Category, Long> {

    List<Category> findByName(String name);
}
