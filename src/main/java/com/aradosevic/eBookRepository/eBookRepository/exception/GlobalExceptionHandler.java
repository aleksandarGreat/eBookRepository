package com.aradosevic.eBookRepository.eBookRepository.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(MultipartException.class)
    public ResponseEntity handleError1(MultipartException e) {
        return new ResponseEntity<>(e.getCause().getMessage(), HttpStatus.OK);
    }

}
