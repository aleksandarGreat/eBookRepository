package com.aradosevic.eBookRepository.eBookRepository.exception;

public class CustomException extends Exception {

    public CustomException(String message) {
        super(message);
    }
}
