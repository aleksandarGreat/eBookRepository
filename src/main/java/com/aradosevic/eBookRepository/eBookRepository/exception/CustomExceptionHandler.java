package com.aradosevic.eBookRepository.eBookRepository.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(value = {CustomException.class})
    public static ResponseEntity handleException(CustomException customException, HttpStatus httpStatus) {
        return new ResponseEntity<Object>(customException.getMessage(), httpStatus);
    }
}
