package com.aradosevic.eBookRepository.eBookRepository.config;

import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.NodeBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.File;
import java.io.IOException;

@Configuration
@EnableJpaRepositories(basePackages = "com.aradosevic.eBookRepository.eBookRepository.repository.jpa")
@EnableElasticsearchRepositories(basePackages = "com.aradosevic.eBookRepository.eBookRepository.repository.elasticsearch")
public class ElasticSearchConfig {

    @Bean
    public NodeBuilder nodeBuilder() {
        return new NodeBuilder();
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() throws IOException {
        File tempDir = File.createTempFile("elastic", Long.toString(System.nanoTime()));
        System.out.println("Temp directory: " + tempDir.getAbsolutePath());

        Settings.Builder elasticsearchSettings =
                Settings.settingsBuilder()
                    .put("http.enabled", "true")
                    .put("index.number_of_shards", "1")
                    .put("path.data", new File(tempDir, "data").getAbsolutePath())
                    .put("path.logs", new File(tempDir, "logs").getAbsolutePath())
                    .put("path.work", new File(tempDir, "work").getAbsolutePath())
                    .put("path.home", tempDir);

        return new ElasticsearchTemplate(nodeBuilder()
                                            .local(true)
                                            .settings(elasticsearchSettings.build())
                                            .node()
                                            .client());
    }
}
