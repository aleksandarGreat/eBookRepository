# e Book Repository

University project mainly focused on successful implementation of Elastic Search with web support.
Spring Boot was used as a framework for server-side implementation

Elastich search library for Spring Boot was used for easier Lucene implementation.
All services are REST-fully exposed and can be tested via Postman.

## Build

Using IntelliJ it can be run through its IDE or after build, jar or war files respectfully can be found in target folder.

## Front-end

This project represents only server side of the overall project, frontend can be found here:
https://gitlab.com/aleksandarGreat/udd-front
